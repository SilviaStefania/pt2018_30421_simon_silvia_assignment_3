package pt_homework3.BusinessLogic;

import pt_homework3.DataAccess.ClientsDAO;
import pt_homework3.DataAccess.ProductsDAO;
import pt_homework3.DataAccess.StoreorderDAO;
import pt_homework3.Model.Clients;
import pt_homework3.Model.Products;
import pt_homework3.Model.Storeorder;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class ValidateProducts {

    private String mess;

    public String getMess() {
        return mess;
    }


    /**
     * Method used to find a correct product by its id
     * @param id
     * @return
     * @throws SQLException
     * @throws IntrospectionException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     */
    public Products validatePrFindById(int id) throws SQLException, IntrospectionException, IllegalAccessException, InvocationTargetException {
        ProductsDAO productsDAO = new ProductsDAO();
        List<Products> cl = productsDAO.getProducts();
        Products result = null;

        Iterator<Products> it = cl.iterator();
        while (it.hasNext()) {
            Products aux = it.next();
            if (aux.getId() == id) {
                result = productsDAO.findProductById(id);
                mess = result.getId() + " " + result.getCategory() + " " + result.getName() + " " + result.getQuantity() + " " + result.getPrice();
                return result;
            }
        }

        mess = "No such id";
        return result;

    }

    /**
     * Method used to insert a product
     * @param category
     * @param name
     * @param quantity
     * @param price
     * @return
     * @throws SQLException
     * @throws IntrospectionException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     */
    public List<Products> insertProd(String category, String name, int quantity, int price) throws SQLException, IntrospectionException, IllegalAccessException, InvocationTargetException {
        ProductsDAO prt = new ProductsDAO();
        List<Products> pr = prt.insertProductDB(category, name, quantity, price);
        mess = "Insertion ok";
        return pr;
    }


    /**
     * Method used to delete a product
     * @param id
     * @return
     * @throws SQLException
     * @throws IntrospectionException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     */
    public List<Products> deletePr(int id) throws SQLException, IntrospectionException, IllegalAccessException, InvocationTargetException {
        ProductsDAO productsDAO = new ProductsDAO();
        List<Products> cl  = productsDAO.getProducts();
        List<Products> okcl = new ArrayList<>();
        String message;

        for (Products aux : cl) {
            if (aux.getId() == id) {
                okcl = productsDAO.deleteProductDB(id);
                mess = "Valid id, Deletion done";
                return okcl;
            }
        }
                mess = "NO such id";
                return cl;

    }

    /**
     * Method used to edit a product
     * @param id
     * @param x
     * @param i
     * @return
     * @throws SQLException
     * @throws IntrospectionException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     */
    public List<Products> validatePrUpdate(int id, String x, String i) throws SQLException, IntrospectionException, IllegalAccessException, InvocationTargetException {
        ProductsDAO productsDAO = new ProductsDAO();
        List<Products> cl  = productsDAO.getProducts();
        List<Products> okcl = new ArrayList<>();


        for (Products aux : cl) {
            if (aux.getId() == id) {
                okcl = productsDAO.updateProductDB(id, x, i);
                mess = "Valid id, Update done";
                return okcl;
            }
        }
                mess = "NO such id";
                return cl;

    }



}
