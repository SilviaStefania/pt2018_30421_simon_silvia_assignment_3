package pt_homework3.BusinessLogic;

import pt_homework3.DataAccess.ClientsDAO;
import pt_homework3.DataAccess.ProductsDAO;
import pt_homework3.DataAccess.StoreorderDAO;
import pt_homework3.Model.Clients;
import pt_homework3.Model.Storeorder;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * This class si responsible for checking if an operation can be done, if the corresponding item exists.
 */
public class ValidateClient {




    private String mess;

    public String getMess() {
        return mess;
    }

    public ValidateClient() throws SQLException, IntrospectionException, IllegalAccessException, InvocationTargetException {


    }

    /**
     * Method used to find a client by its id
     * @param id
     * @return The client corresponding to the id is returned
     * @throws SQLException
     * @throws IntrospectionException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     */
    public Clients validateClFindById(int id) throws SQLException, IntrospectionException, IllegalAccessException, InvocationTargetException {
        ClientsDAO clientsDAO = new ClientsDAO();
        List<Clients> cl  = clientsDAO.getClients();
        Clients result = null;

        Iterator<Clients> it = cl.iterator();
        while (it.hasNext()) {
            Clients aux = it.next();
            if (aux.getId() == id) {
                result = (Clients) clientsDAO.findById(id);
                mess=result.getId()+"  "+result.getName()+"  "+result.getEmail()+"  "+result.getAddress();
                return aux;
            }
        }
        mess = "No such id";

return null;

    }

    /**
     * Method used to insert correct client
     * @param name
     * @param email
     * @param address
     * @return The new list, containing all the clients
     * @throws SQLException
     * @throws IntrospectionException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     */
    public List<Clients> insertClient(String name, String email, String address) throws SQLException, IntrospectionException, IllegalAccessException, InvocationTargetException {
        ClientsDAO clie = new ClientsDAO();
        List<Clients> cl = clie.insertClientDB(name, email, address);
        mess = "Insert ok";
        return cl;
    }


    /**
     * Method used to delete a correct client
     * @param id
     * @return The new list, containing all the clients
     * @throws SQLException
     * @throws IntrospectionException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     */
    public List<Clients> deleteClient(int id) throws SQLException, IntrospectionException, IllegalAccessException, InvocationTargetException {
        ClientsDAO clientsDAO = new ClientsDAO();
        List<Clients> cl  = clientsDAO.getClients();
        List<Clients> okcl = new ArrayList<>();

        for (Clients aux : cl) {
            if (aux.getId() == id) {
                 clientsDAO.deleteClientDB(id);
                mess = "Valid id, Deletion ok";
                return okcl;
            }
        }
                mess = "NO such id";
                return cl;

    }

    /**
     *  Method used to edit a correct client
     * @param id
     * @param fieldtoUp
     * @param newValue
     * @return
     * @throws SQLException
     * @throws IntrospectionException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     */
    public List<Clients> ClUpdate(int id, String fieldtoUp, String newValue) throws SQLException, IntrospectionException, IllegalAccessException, InvocationTargetException {
        ClientsDAO clientsDAO = new ClientsDAO();
        List<Clients> cl  = clientsDAO.getClients();
        List<Clients> okcl = new ArrayList<>();

        for (Clients aux : cl) {
            if (aux.getId() == id) {
                okcl = clientsDAO.updateClientDB(id, fieldtoUp, newValue);
                mess = "Valid id, Update ok";
                return okcl;
            }
        }
        mess = "NO such id";
        return cl;

    }







}
