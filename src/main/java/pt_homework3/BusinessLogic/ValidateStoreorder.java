package pt_homework3.BusinessLogic;

import pt_homework3.DataAccess.ClientsDAO;
import pt_homework3.DataAccess.ProductsDAO;
import pt_homework3.DataAccess.StoreorderDAO;
import pt_homework3.DataAccess.StoreorderDAO;
import pt_homework3.Model.Clients;
import pt_homework3.Model.Products;
import pt_homework3.Model.Storeorder;
import pt_homework3.Model.Storeorder;

import java.beans.Customizer;
import java.beans.IntrospectionException;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * This class si responsible for checking if an operation can be done, if the corresponding item exists.
 */
public class ValidateStoreorder {

    private String mess;

    public String getMess() {
        return mess;
    }

    /**
     * Method used to create the bill for an order
     * @param clientId
     * @throws SQLException
     * @throws IntrospectionException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     * @throws FileNotFoundException
     * @throws UnsupportedEncodingException
     */
    public void makeBill (int clientId) throws SQLException, IntrospectionException, IllegalAccessException, InvocationTargetException, FileNotFoundException, UnsupportedEncodingException {
        ProductsDAO p = new ProductsDAO();
        ClientsDAO c = new ClientsDAO();
        StoreorderDAO s = new StoreorderDAO();
        List<Clients> findCustomerById = c.getClients();
        List<Products> prtFull =  p.getProducts();
        List<Storeorder> orderss = s.getStoreorder();
        int price = 0;
        String clientName = "";
        Iterator<Clients> itr = findCustomerById.iterator();
        while (itr.hasNext()){
            Clients clo = itr.next();
            if (clo.getId() == clientId ){
                clientName = clo.getName();
            }
        }

int ok = 0;

        String prod = "Product(s) ordered: \n ";
        PrintWriter writer = new PrintWriter("tema3.txt");
        writer.println("                    BILL");
        String custName = "Customer name: " + clientName;
        writer.println(custName);


        Iterator<Storeorder> it = orderss.iterator();
        while (it.hasNext()){
            Storeorder aux = it.next();
            if(aux.getClientID()==clientId){
                Iterator<Products> itt = prtFull.iterator();
                while (itt.hasNext()){
                    Products au = itt.next();
                    if(au.getId() == aux.getProductID()){
                        prod="\n Category :"+ au.getCategory()+" Name: "+au.getName()+" Quantity ordered: "+aux.getQuantity()+"\n";
                        writer.println(prod);
                        price+=aux.getQuantity() * au.getPrice();
                        ok=1;
                    }
                }

            }

        }



        for (Clients aux : findCustomerById) {
            if (aux.getId() == clientId) {
                clientName = aux.getName();
            }

        }
        if (ok == 1) {



            String pr ="";
            pr+=price;
            writer.println("                                                Total price: "+pr);
            writer.close();
            mess="OK";
        }else {

            writer.println(" No such client");
            mess = "No such client";
        }



    }


    /**
     * Method used to find a order by its id
     * @param id
     * @return
     * @throws SQLException
     * @throws IntrospectionException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     */
    public Storeorder validateOrderFindById(int id) throws SQLException, IntrospectionException, IllegalAccessException, InvocationTargetException {
        StoreorderDAO storeorderDAO = new StoreorderDAO();
        List<Storeorder> cl  = storeorderDAO.getStoreorder();
        Storeorder result = null;

        Iterator<Storeorder> it = cl.iterator();
        while (it.hasNext()){
            Storeorder aux =  it.next();
            if(aux.getId() == id){
                result = storeorderDAO.findStoreById(id);
                mess=aux.getId()+" "+aux.getClientID()+" "+aux.getProductID()+" "+aux.getQuantity();
                return result;
            }
        }

        mess= "Id does not exit";

        return result;

    }


    /**
     * Method used to insert an order
     * @param clientID
     * @param productID
     * @param quantity
     * @return
     * @throws SQLException
     * @throws IntrospectionException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     */
    public List<Storeorder> insertOrder(int clientID, int productID, int quantity) throws SQLException, IntrospectionException, IllegalAccessException, InvocationTargetException {
        StoreorderDAO storeorderDAO = new StoreorderDAO();
        List<Storeorder> cl = new ArrayList<>();
        ClientsDAO clientsDAO = new ClientsDAO();
        mess="*";
        List<Clients> auxcl = clientsDAO.getClients();
        Iterator<Clients> irt = auxcl.iterator();
        while (irt.hasNext()){
            Clients aa = irt.next();
            if(aa.getId() == clientID ){
                ProductsDAO productsDAO = new ProductsDAO();
                List<Products> prod = productsDAO.getProducts();
                Iterator<Products> itt = prod.iterator();
                while (itt.hasNext()){
                    Products p = itt.next();
                    if(p.getId() == productID){
                        if(p.getQuantity() < quantity){
                            mess="Stack under flow";
                            return cl;
                        } else{
                            mess = "Insertion OK, enough quantity";
                            cl = storeorderDAO.insertOrderDB(clientID, productID, quantity);
                            int y = p.getQuantity();
                            p.setQuantity(y - quantity);
                            return cl;
                        }
                    }

                }mess = "No such product";
                return cl;
            }
        } mess+=" no such client";

        return cl;
    }

    /**
     * Method used to delete an order
     * @param id
     * @return
     * @throws SQLException
     * @throws IntrospectionException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     */
    public List<Storeorder> deleteOrder(int id) throws SQLException, IntrospectionException, IllegalAccessException, InvocationTargetException {
        StoreorderDAO storeorderDAO = new StoreorderDAO();
        List<Storeorder> cl  = storeorderDAO.getStoreorder();
        List<Storeorder> okcl = new ArrayList<>();

        for (Storeorder aux : cl) {
            if (aux.getId() == id) {
                okcl = storeorderDAO.deleteOrderDB(id);
                mess = "Valid id, Deletion ok";
                return okcl;
            }
        }
                mess = "NO such id";
                return cl;

    }

    /**
     * Method used to update an order
     * @param id
     * @param x
     * @param i
     * @return
     * @throws SQLException
     * @throws IntrospectionException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     */
    public List<Storeorder> validateOrderUpdate(int id, String x, String i) throws SQLException, IntrospectionException, IllegalAccessException, InvocationTargetException {
        StoreorderDAO storeorderDAO = new StoreorderDAO();
        List<Storeorder> cl  = storeorderDAO.getStoreorder();
        List<Storeorder> okcl = new ArrayList<>();
        for (Storeorder aux : cl) {
            if (aux.getId() == id) {
                okcl = storeorderDAO.updateOrderDB(id, x, i);
                mess = "Valid id, Update done";
                return okcl;
            }
        }
                mess = "NO such id";
                return cl;
    }



}
