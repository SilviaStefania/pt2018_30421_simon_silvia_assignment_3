package pt_homework3.Model;
/**
 * The class representing a order from the database
 */
public class Storeorder {

   private int id;
    private int clientID;
    private int productID;
    private int quantity;

    public Storeorder (int id, int clientID, int productID, int quantity){
        this.id = id;
        this.clientID = clientID;
        this.productID=productID;
        this.quantity = quantity;
    }

    public Storeorder (int clientID, int productID, int quantity){

        this.clientID = clientID;
        this.productID=productID;
        this.quantity = quantity;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setClientID(int clientID) {
        this.clientID = clientID;
    }


    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getClientID() {
        return clientID;
    }


    public void setProductID(int productID) {
        this.productID = productID;
    }


    public int getProductID() {
        return productID;
    }
}
