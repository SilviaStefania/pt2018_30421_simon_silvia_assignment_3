package pt_homework3.Model;

import java.lang.reflect.Field;

/**
 * The class representing a client from the database
 */
public class Clients {

    private int id;
    private String name;
    private String email;
    private String address;


    public Clients(int id, String name, String email, String address){
        this.id=id;
        this.name = name;
        this.email = email;
        this.address =address;
    }

    public Clients(String name, String email, String address){
        this.name = name;
        this.email = email;
        this.address =address;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



}
