package pt_homework3.Presentation;

import pt_homework3.BusinessLogic.ValidateClient;
import pt_homework3.BusinessLogic.ValidateProducts;
import pt_homework3.DataAccess.ClientsDAO;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;

public class ProductViewController {

    private ProductView view;

    public ProductViewController (ProductView view) {
        this.view = view;

        this.view.addBackListener(new backButtonListener() );
        this.view.addAddListener(new addListener());
        this.view.addDeleteListener(new deleteButtonListener());
        this.view.addUPDATEListener(new updateButtonListener());
        this.view.addFindByIDListener(new findAfterIDButtonListener());
        this.view.addVIEWALLCLIENTSListener(new findAllButtonListener());

    }

    class backButtonListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            //cod care deschide fereastra noua de management client
            //PUNE BUTON DE BACK
            view.dispose();
            StartView startView = new StartView();
            startView.setVisible(true);
            StartViewController startViewController = new StartViewController(startView);

        }
    }


    class addListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            try {

                ValidateProducts validateProducts= new ValidateProducts();
                validateProducts.insertProd(view.getNameAddText().getText(), view.nameAddText3().getText(), Integer.parseInt(view.getQuantAddText().getText()),Integer.parseInt( view.getPriceText().getText()));
                view.setResultAll(validateProducts.getMess());
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (IntrospectionException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }


        }
    }

    class deleteButtonListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            try {

                ValidateProducts validateProducts= new ValidateProducts();
                validateProducts.deletePr(Integer.parseInt(view.getIdDeleteText().getText()));
                view.setResultAll(validateProducts.getMess());
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (IntrospectionException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }

        }
    }


    class updateButtonListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            try {

                ValidateProducts validateProducts= new ValidateProducts();
                validateProducts.validatePrUpdate(Integer.parseInt(view.getIdUpTezt().getText()), view.getFieldToUpTextEdit().getText(), view.getNewValueTextEdit().getText());
                view.setResultAll(validateProducts.getMess());
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (IntrospectionException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }

        }
    }


    class findAfterIDButtonListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            try {

                ValidateProducts validateProducts= new ValidateProducts();
                validateProducts.validatePrFindById(Integer.parseInt(view.getIdFindBuIdText().getText()));
                view.setResultAll(validateProducts.getMess());
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (IntrospectionException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }




        }
    }

    class findAllButtonListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            view.dispose();
            ProductView cl = null;
            try {
                cl = new ProductView();
                cl.setVisible(true);
                ProductViewController clientViewController = new ProductViewController(cl);

            } catch (SQLException e) {
                e.printStackTrace();
            } catch (IntrospectionException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }

        }
    }


   /*  class backButtonListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            //cod care deschide fereastra noua de management client
            //PUNE BUTON DE BACK
            view.dispose();
            StartView startView = new StartView();
            startView.setVisible(true);
            StartViewController startViewController = new StartViewController(startView);

        }
    }

    class backButtonListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            //cod care deschide fereastra noua de management client
            //PUNE BUTON DE BACK
            view.dispose();
            StartView startView = new StartView();
            startView.setVisible(true);
            StartViewController startViewController = new StartViewController(startView);

        }
    }
    */

}
