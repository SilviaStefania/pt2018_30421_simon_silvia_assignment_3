package pt_homework3.Presentation;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.lang.Object;
import java.util.List;
import java.util.Objects;

/**
 * Class used to get the header of the table and the rest of the information from the database using reflection.
 */
public class Refl {

    private Object[][] data ;
    private String[] columnName ;


    public Refl(ArrayList<Object> objects, int nrOfRows,  int nrOfColumns){

       Object[][] data =new Object[nrOfRows*2][nrOfColumns];
       String[] columnName = new String[nrOfColumns];
        int index = 0;
        for(Field field : objects.get(1).getClass().getDeclaredFields()){
            field.setAccessible(true);
            try{
                columnName[index] = field.getName();
                //System.out.println(columnName[index] + "////");
            }catch (IllegalArgumentException e){
                e.printStackTrace();
            }
            index ++;
        }
        this.columnName =  columnName;
        int j = 0 ;
        int u = 0;
        Iterator<Object> itt= objects.iterator();
        while(itt.hasNext()){

            Object object = itt.next();
         //for(int u = 0; u<nrOfRows; u++){
            j = 0;
                for(Field field : object.getClass().getDeclaredFields()){
                    field.setAccessible(true);
                    Object value;
                    try{
                        value = field.get(object);
                        data[u][j] = value;
                        //System.out.println( data[u][j]+"**////****"+j+u);
                        j++;
                    }
                    catch (IllegalArgumentException | IllegalAccessException e) {
                        e.printStackTrace();
                    }

                }
            u++;
         // }
        }
        this.data  =data;



    }

    /**
     * @return the data form the table
     */
    public Object[][] getData() {

        return data;
    }

    /**
     * @return the column name
     */
    public String[] getColumnName() {

        return columnName;
    }
}
