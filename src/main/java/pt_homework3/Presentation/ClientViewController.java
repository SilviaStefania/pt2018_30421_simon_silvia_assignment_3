package pt_homework3.Presentation;

import pt_homework3.BusinessLogic.ValidateClient;
import pt_homework3.DataAccess.ClientsDAO;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.lang.Object;
import java.util.Objects;

public class ClientViewController {

    private ClientView view;

    public ClientViewController (ClientView view) {
        this.view = view;

        this.view.addBackListener(new backButtonListener() );
        this.view.addAddListener(new addListener());
        this.view.addDeleteListener(new deleteButtonListener());
        this.view.addUPDATEListener(new updateButtonListener());
        this.view.addFindByIDListener(new findAfterIDButtonListener());
        this.view.addVIEWALLCLIENTSListener(new findAllButtonListener());

    }

    class backButtonListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            //cod care deschide fereastra noua de management client
            //PUNE BUTON DE BACK
            view.dispose();
            StartView startView = new StartView();
            startView.setVisible(true);
            StartViewController startViewController = new StartViewController(startView);

        }
    }


    class addListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            try {

                ValidateClient validateClient= new ValidateClient();
                validateClient.insertClient(view.getNameAddText().getText(), view.getEmailAddText().getText(), view.getAddressAddText().getText());
                view.setResultAll(validateClient.getMess());
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (IntrospectionException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }


        }
    }

      class deleteButtonListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            try {

                ValidateClient validateClient= new ValidateClient();
                validateClient.deleteClient(Integer.parseInt(view.getIdDeleteText().getText()));
                view.setResultAll(validateClient.getMess());
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (IntrospectionException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }

        }
    }


    class updateButtonListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            try {

                ValidateClient validateClient= new ValidateClient();
                validateClient.ClUpdate(Integer.parseInt(view.getIdUpTezt().getText()), view.getFieldToUpTextEdit().getText(), view.getNewValueTextEdit().getText());
                view.setResultAll(validateClient.getMess());
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (IntrospectionException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }

        }
    }


    class findAfterIDButtonListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            try {

                ValidateClient validateClient= new ValidateClient();
                validateClient.validateClFindById(Integer.parseInt(view.getIdFindBuIdText().getText()));
                view.setResultAll(validateClient.getMess());
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (IntrospectionException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }




        }
    }



   class findAllButtonListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            view.dispose();
            ClientView cl = null;
            try {
                cl = new ClientView();
                cl.setVisible(true);
                ClientViewController clientViewController = new ClientViewController(cl);

            } catch (SQLException e) {
                e.printStackTrace();
            } catch (IntrospectionException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }

        }
    }




}
