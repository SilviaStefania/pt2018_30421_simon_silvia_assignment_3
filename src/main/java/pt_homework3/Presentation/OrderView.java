package pt_homework3.Presentation;

import pt_homework3.DataAccess.ProductsDAO;
import pt_homework3.DataAccess.StoreorderDAO;
import pt_homework3.Model.Storeorder;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;

public class OrderView extends JFrame {

    private JButton back = new JButton("Back");
    private JLabel mess1 = new JLabel("Manage Orderes");
    private JButton addLable = new JButton("Add");
    private JLabel idLable = new JLabel("ID");
    private JLabel cleintIDLable = new JLabel("ClienID");
    private JLabel prodIDlable = new JLabel("ProductID");
    private JLabel quantity = new JLabel("Quantity");

    private JButton editLable = new JButton("Edit");
    
    private JLabel fieldToUpLable = new JLabel("Field to update");
    private JLabel newValueLabel = new JLabel("New value");
    private JButton findByIdLAbel = new JButton("Find after ID");
    private JButton deleteLabel = new JButton("Delete");
    private JButton viewAllProdButton = new JButton("List all products");

    private JButton bill =  new JButton("Bill");
    private JTextField enterBill = new JTextField("Client id", 5);

    private JTextField cloeintIdText = new JTextField(10);
    private JTextField prodIdText = new JTextField(10);
    private JTextField quantAddText = new JTextField(10);
    private JTextField fieldToUpTextEdit = new JTextField(10);
    private JTextField newValueTextEdit = new JTextField(10);
    private JTextField idDeleteText = new JTextField(5);
    private JTextField idUpTezt = new JTextField(5);
    private JTextField idFindBuIdText = new JTextField(5);
    private JTextField resultAll = new JTextField(20);

    public String getCloeintIdText() {
        return cloeintIdText.getText();
    }

    public String getProdIdText() {
        return prodIdText.getText();
    }




    StoreorderDAO storeorderDAO = new StoreorderDAO();
    Refl refl = new Refl(storeorderDAO.getMakeObjList(), storeorderDAO.getNrOfRows(), 4 );

    private JTable table = new JTable(refl.getData(), refl.getColumnName());

    private JScrollPane scrollPane = new JScrollPane(table);



    GridBagConstraints aranjare = new GridBagConstraints();

    public OrderView() throws SQLException, IntrospectionException, IllegalAccessException, InvocationTargetException {
        JPanel calcPanel =  new JPanel(new GridBagLayout());
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(1500, 2000);
        this.setTitle("Order");

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 0;
        aranjare.gridx = 0;
        calcPanel.add(back, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 2;
        aranjare.gridx = 1;
        calcPanel.add(addLable, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 1;
        aranjare.gridx = 2;
        calcPanel.add(cleintIDLable, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 1;
        aranjare.gridx = 3;
        calcPanel.add(prodIDlable, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 1;
        aranjare.gridx = 4;
        calcPanel.add(quantity, aranjare);


        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 2;
        aranjare.gridx = 2;
        calcPanel.add(cloeintIdText, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 2;
        aranjare.gridx = 3;
        calcPanel.add(prodIdText, aranjare);


        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 2;
        aranjare.gridx = 4;
        calcPanel.add(quantAddText, aranjare);
        

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 4;
        aranjare.gridx = 1;
        calcPanel.add(editLable, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 3;
        aranjare.gridx = 2;
        calcPanel.add(idLable, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 3;
        aranjare.gridx = 3;
        calcPanel.add(fieldToUpLable, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 3;
        aranjare.gridx = 4;
        calcPanel.add(newValueLabel, aranjare);


        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 4;
        aranjare.gridx = 2;
        calcPanel.add(idUpTezt, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 4;
        aranjare.gridx = 3;
        calcPanel.add(fieldToUpTextEdit, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 4;
        aranjare.gridx = 4;
        calcPanel.add(newValueTextEdit, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 6;
        aranjare.gridx = 1;
        calcPanel.add(deleteLabel, aranjare);


        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 6;
        aranjare.gridx = 2;
        calcPanel.add(idDeleteText, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 8;
        aranjare.gridx = 1;
        calcPanel.add(findByIdLAbel, aranjare);


        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 8;
        aranjare.gridx = 2;
        calcPanel.add(idFindBuIdText, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 9;
        aranjare.gridx = 1;
        calcPanel.add(viewAllProdButton, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 10;
        aranjare.gridx = 1;
        calcPanel.add(bill, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 10;
        aranjare.gridx = 2;
        calcPanel.add(enterBill, aranjare);



        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 12;
        aranjare.gridx = 3;
        calcPanel.add(resultAll, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 13;
        aranjare.gridx = 3;
        calcPanel.add(scrollPane, aranjare);




        this.add(calcPanel);
        this.setVisible(true);


    }



    public JTextField getNameAddText() {
        return cloeintIdText;
    }

    public void setNameAddText(JTextField nameAddText) {
        this.cloeintIdText = nameAddText;
    }

    public void setQuantAddText(JTextField quantAddText) {
        this.quantAddText = quantAddText;
    }

    public String getQuantAddText() {
        return quantAddText.getText();
    }

    public JTextField nameAddText3() {
        return prodIdText;
    }


    public JTextField getFieldToUpTextEdit() {
        return fieldToUpTextEdit;
    }

    public void setFieldToUpTextEdit(JTextField fieldToUpTextEdit) {
        this.fieldToUpTextEdit = fieldToUpTextEdit;
    }

    public JTextField getNewValueTextEdit() {
        return newValueTextEdit;
    }

    public void setNewValueTextEdit(JTextField newValueTextEdit) {
        this.newValueTextEdit = newValueTextEdit;
    }

    public JTextField getIdDeleteText() {
        return idDeleteText;
    }

    public void setIdDeleteText(JTextField idDeleteText) {
        this.idDeleteText = idDeleteText;
    }

    public JTextField getIdFindBuIdText() {
        return idFindBuIdText;
    }

    public void setIdFindBuIdText(JTextField idFindBuIdText) {
        this.idFindBuIdText = idFindBuIdText;
    }

    public JTextField getIdUpTezt() {
        return idUpTezt;
    }

    public void setIdUpTezt(JTextField idUpTezt) {
        this.idUpTezt = idUpTezt;
    }

    public JTextField getResultAll() {
        return resultAll;
    }

    public void setResultAll(String resultAll) {
        this.resultAll.setText(resultAll);
    }

    public String getEnterBill() {
        return enterBill.getText();
    }

    public void addBackListener(ActionListener listenForBackButton){
        back.addActionListener(listenForBackButton);
    }

    public void addDeleteListener(ActionListener x){
        deleteLabel.addActionListener(x);
    }

    public void addAddListener(ActionListener listenForBackButton){
        addLable.addActionListener(listenForBackButton);
    }

    public void addFindByIDListener(ActionListener x){
        findByIdLAbel.addActionListener(x);
    }

    public void addUPDATEListener(ActionListener x){
        editLable.addActionListener(x);
    }

    public void addVIEWALLCLIENTSListener(ActionListener x){
        viewAllProdButton.addActionListener(x);
    }

    public void addBillLis(ActionListener x){
        bill.addActionListener(x);
    }

 /*   public void addHOMECONTListener(ActionListener listenForBackButton){
        back.addActionListener(listenForBackButton);
    }

    public void addNTHOMEListener(ActionListener listenForBackButton){
        back.addActionListener(listenForBackButton);
    }

*/




}
