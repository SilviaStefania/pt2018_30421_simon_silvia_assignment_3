package pt_homework3.Presentation;

import pt_homework3.DataAccess.ClientsDAO;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.beans.IntrospectionException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ClientView extends JFrame {



    private JButton back = new JButton("Back");
    private JLabel mess1 = new JLabel("Manage clients");
    private JButton addLable = new JButton("Add");
    private JLabel idLable = new JLabel("ID");
    private JLabel nameLable = new JLabel("Name");
    private JLabel emailLable = new JLabel("Email");
    private JLabel addressLable = new JLabel("Address");
    private JButton editLable = new JButton("Edit");


    private JLabel fieldToUpLable = new JLabel("Field to update");
    private JLabel newValueLabel = new JLabel("New value");
    private JButton findByIdLAbel = new JButton("Find after ID");
    private JButton deleteLabel = new JButton("Delete");
    private JButton viewAllClientsButoon = new JButton("List all clients");
    private JLabel filterLable = new JLabel("Filter for shipping");
    private JButton homeButton = new JButton("HomeContinent");
    private JButton notHomeButton = new JButton("OtherContinent");

    private JTextField nameAddText = new JTextField(10);
    private JTextField emailAddText = new JTextField(10);
    private JTextField addressAddText = new JTextField(10);
    private JTextField fieldToUpTextEdit = new JTextField(10);
    private JTextField newValueTextEdit = new JTextField(10);
    private JTextField idDeleteText = new JTextField(5);
    private JTextField idUpTezt = new JTextField(5);
    private JTextField idFindBuIdText = new JTextField(5);
    private JTextField resultAll = new JTextField(20);




   // private JTable table = new JTable(data, columnName);


    ClientsDAO clientsDAO = new ClientsDAO();
    Refl refl = new Refl(clientsDAO.getMakeObjList(), clientsDAO.getNrOfRows(), 4 );

    private JTable table = new JTable(refl.getData(), refl.getColumnName());

    private JScrollPane scrollPane = new JScrollPane(table);



    GridBagConstraints aranjare = new GridBagConstraints();

    public ClientView() throws SQLException, IntrospectionException, IllegalAccessException, InvocationTargetException {
        JPanel calcPanel =  new JPanel(new GridBagLayout());
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(1500, 2000);
        this.setTitle("Clients");
        homeButton.setToolTipText("Find clients that are in Europe");

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 0;
        aranjare.gridx = 0;
        calcPanel.add(back, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 2;
        aranjare.gridx = 1;
        calcPanel.add(addLable, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 1;
        aranjare.gridx = 2;
        calcPanel.add(nameLable, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 1;
        aranjare.gridx = 3;
        calcPanel.add(emailLable, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 1;
        aranjare.gridx = 4;
        calcPanel.add(addressLable, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 2;
        aranjare.gridx = 2;
        calcPanel.add(nameAddText, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 2;
        aranjare.gridx = 3;
        calcPanel.add(emailAddText, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 2;
        aranjare.gridx = 4;
        calcPanel.add(addressAddText, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 4;
        aranjare.gridx = 1;
        calcPanel.add(editLable, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 3;
        aranjare.gridx = 2;
        calcPanel.add(idLable, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 3;
        aranjare.gridx = 3;
        calcPanel.add(fieldToUpLable, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 3;
        aranjare.gridx = 4;
        calcPanel.add(newValueLabel, aranjare);


        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 4;
        aranjare.gridx = 2;
        calcPanel.add(idUpTezt, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 4;
        aranjare.gridx = 3;
        calcPanel.add(fieldToUpTextEdit, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 4;
        aranjare.gridx = 4;
        calcPanel.add(newValueTextEdit, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 6;
        aranjare.gridx = 1;
        calcPanel.add(deleteLabel, aranjare);


        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 6;
        aranjare.gridx = 2;
        calcPanel.add(idDeleteText, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 8;
        aranjare.gridx = 1;
        calcPanel.add(findByIdLAbel, aranjare);


        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 8;
        aranjare.gridx = 2;
        calcPanel.add(idFindBuIdText, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 9;
        aranjare.gridx = 1;
        calcPanel.add(viewAllClientsButoon, aranjare);


        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 12;
        aranjare.gridx = 3;
        calcPanel.add(resultAll, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 13;
        aranjare.gridx =2;
        calcPanel.add(scrollPane, aranjare);



        this.add(calcPanel);
        this.setVisible(true);


    }


    public JTextField getNameAddText() {
        return nameAddText;
    }

    public void setNameAddText(JTextField nameAddText) {
        this.nameAddText = nameAddText;
    }

    public void setAddressAddText(JTextField addressAddText) {
        this.addressAddText = addressAddText;
    }

    public JTextField getAddressAddText() {
        return addressAddText;
    }

    public JTextField getEmailAddText() {
        return emailAddText;
    }

    public void setEmailAddText(JTextField emailAddText) {
        this.emailAddText = emailAddText;
    }

    public JTextField getFieldToUpTextEdit() {
        return fieldToUpTextEdit;
    }

    public void setFieldToUpTextEdit(JTextField fieldToUpTextEdit) {
        this.fieldToUpTextEdit = fieldToUpTextEdit;
    }

    public JTextField getNewValueTextEdit() {
        return newValueTextEdit;
    }

    public void setNewValueTextEdit(JTextField newValueTextEdit) {
        this.newValueTextEdit = newValueTextEdit;
    }

    public JTextField getIdDeleteText() {
        return idDeleteText;
    }

    public void setIdDeleteText(JTextField idDeleteText) {
        this.idDeleteText = idDeleteText;
    }

    public JTextField getIdFindBuIdText() {
        return idFindBuIdText;
    }

    public void setIdFindBuIdText(JTextField idFindBuIdText) {
        this.idFindBuIdText = idFindBuIdText;
    }

    public JTextField getIdUpTezt() {
        return idUpTezt;
    }

    public void setIdUpTezt(JTextField idUpTezt) {
        this.idUpTezt = idUpTezt;
    }

    public JTextField getResultAll() {
        return resultAll;
    }

    public void setResultAll(String resultAll) {
        this.resultAll.setText(resultAll);
    }


    public void addBackListener(ActionListener listenForBackButton){
        back.addActionListener(listenForBackButton);
    }

   public void addDeleteListener(ActionListener x){
       deleteLabel.addActionListener(x);
   }

    public void addAddListener(ActionListener listenForBackButton){
        addLable.addActionListener(listenForBackButton);
    }

   public void addFindByIDListener(ActionListener x){
        findByIdLAbel.addActionListener(x);
   }

    public void addUPDATEListener(ActionListener x){
        editLable.addActionListener(x);
    }

    public void addVIEWALLCLIENTSListener(ActionListener x){
       viewAllClientsButoon.addActionListener(x);
    }




}
