package pt_homework3.Presentation;

import pt_homework3.BusinessLogic.ValidateClient;
import pt_homework3.BusinessLogic.ValidateProducts;
import pt_homework3.BusinessLogic.ValidateStoreorder;
import pt_homework3.DataAccess.ClientsDAO;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.IntrospectionException;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;

public class OrderViewController {

    private OrderView view;

    public OrderViewController (OrderView view) {
        this.view = view;

        this.view.addBackListener(new backButtonListener() );
        this.view.addAddListener(new addListener());
        this.view.addDeleteListener(new deleteButtonListener());
        this.view.addUPDATEListener(new updateButtonListener());
        this.view.addFindByIDListener(new findAfterIDButtonListener());
        this.view.addVIEWALLCLIENTSListener(new findAfterIDButtonListener());
        this.view.addBillLis(new billl() );
        this.view.addVIEWALLCLIENTSListener(new findAllButtonListener ());


    }

    class backButtonListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            //cod care deschide fereastra noua de management client
            //PUNE BUTON DE BACK
            view.dispose();
            StartView startView = new StartView();
            startView.setVisible(true);
            StartViewController startViewController = new StartViewController(startView);

        }
    }


    class addListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            try {

                ValidateStoreorder validateStoreorder= new ValidateStoreorder();
                validateStoreorder.insertOrder(Integer.parseInt(view.getCloeintIdText()), Integer.parseInt(view.getProdIdText()), Integer.parseInt(view.getQuantAddText()));
                view.setResultAll(validateStoreorder.getMess());
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (IntrospectionException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }


        }
    }

    class deleteButtonListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            try {

                ValidateStoreorder validateStoreorder= new ValidateStoreorder();
                validateStoreorder.deleteOrder(Integer.parseInt(view.getIdDeleteText().getText()));
                view.setResultAll(validateStoreorder.getMess());
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (IntrospectionException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }

        }
    }


    class updateButtonListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            try {

                ValidateStoreorder validateStoreorder= new ValidateStoreorder();
                validateStoreorder.validateOrderUpdate(Integer.parseInt(view.getIdUpTezt().getText()), view.getFieldToUpTextEdit().getText(), view.getNewValueTextEdit().getText());
                view.setResultAll(validateStoreorder.getMess());

            } catch (SQLException e) {
                e.printStackTrace();
            } catch (IntrospectionException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }

        }
    }


    class findAfterIDButtonListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            try {

                ValidateStoreorder validateStoreorder= new ValidateStoreorder();
                validateStoreorder.validateOrderFindById(Integer.parseInt(view.getIdFindBuIdText().getText()));
                view.setResultAll(validateStoreorder.getMess());



            } catch (SQLException e) {
                e.printStackTrace();
            } catch (IntrospectionException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }




        }
    }

    class billl implements ActionListener{
        public void actionPerformed(ActionEvent arg0){
            ValidateStoreorder validateStoreorder= new ValidateStoreorder();
            try {
                validateStoreorder.makeBill(Integer.parseInt(view.getEnterBill()));
                view.setResultAll(validateStoreorder.getMess());
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (IntrospectionException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    }

            class findAllButtonListener implements ActionListener {

                public void actionPerformed(ActionEvent arg0) {
                    view.dispose();
                    OrderView cl = null;
                    try {
                        cl = new OrderView();
                        cl.setVisible(true);
                        OrderViewController clientViewController = new OrderViewController(cl);

                    } catch (SQLException e) {
                        e.printStackTrace();
                    } catch (IntrospectionException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    }

                }
            }


   /*  class backButtonListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            //cod care deschide fereastra noua de management client
            //PUNE BUTON DE BACK
            view.dispose();
            StartView startView = new StartView();
            startView.setVisible(true);
            StartViewController startViewController = new StartViewController(startView);

        }
    }

    class backButtonListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            //cod care deschide fereastra noua de management client
            //PUNE BUTON DE BACK
            view.dispose();
            StartView startView = new StartView();
            startView.setVisible(true);
            StartViewController startViewController = new StartViewController(startView);

        }
    }
    */

}
