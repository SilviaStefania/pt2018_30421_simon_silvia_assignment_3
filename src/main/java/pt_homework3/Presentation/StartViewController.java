package pt_homework3.Presentation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;

public class StartViewController{

    private StartView view ;


    public StartViewController (StartView view) {
        this.view = view;

        this.view.addClientOppListener(new ClientButtonListener() );
        this.view.addProductOppListener(new ProdtButtonListener() );
        this.view.addOrderOppListener(new OrderButtonListener());


    }

    class ClientButtonListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            //cod care deschide fereastra noua de management client
            //PUNE BUTON DE BACK
            view.dispose();
            ClientView clientView = null;
            try {
                clientView = new ClientView();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (IntrospectionException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
            clientView.setVisible(true);
            ClientViewController clientViewController = new ClientViewController(clientView);



        }
    }

    class ProdtButtonListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            //cod care deschide fereastra noua de management client
            //PUNE BUTON DE BACK
            view.dispose();
            ProductView productView = null;
            try {
                productView = new ProductView();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (IntrospectionException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
            productView.setVisible(true);
            ProductViewController productViewController = new ProductViewController(productView);
        }
    }

    class OrderButtonListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            //cod care deschide fereastra noua de management client
            //PUNE BUTON DE BACK
            view.dispose();
            OrderView orderView = null;
            try {
                orderView = new OrderView();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (IntrospectionException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
            orderView.setVisible(true);
            OrderViewController orderViewController = new OrderViewController(orderView);
        }
    }

}
