package pt_homework3.Presentation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class StartView  extends JFrame {

    private JLabel welcomeMess = new JLabel( "Welcome!");
    private JLabel selectMess = new JLabel( "Do you want to make an operation on Client or Product?");
    private JLabel selectMess2 = new JLabel( "Or to place a new Order of an exiting client?");

    private JButton clientOpButton = new JButton("Client");
    private JButton prodOpButton = new JButton("Product");
    private JButton orderOpButton = new JButton("Order");

    GridBagConstraints aranjare= new GridBagConstraints();

    public StartView (){
        JPanel calcPanel =  new JPanel(new GridBagLayout());
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(500, 400);
        this.setTitle("DataBase Management");

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 0;
        aranjare.gridx = 1;
        calcPanel.add(welcomeMess, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 1;
        aranjare.gridx = 1;
        calcPanel.add(selectMess, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 2;
        aranjare.gridx = 1;
        calcPanel.add(selectMess2, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 3;
        aranjare.gridx = 1;
        calcPanel.add(clientOpButton, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 4;
        aranjare.gridx = 1;
        calcPanel.add(prodOpButton, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 5;
        aranjare.gridx = 1;
        calcPanel.add(orderOpButton, aranjare);

        this.add(calcPanel);
        this.setVisible(true);
    }


    public void addClientOppListener(ActionListener listenForClientButton){
        clientOpButton.addActionListener(listenForClientButton);
    }

    public void addProductOppListener(ActionListener listenForProdButton){
        prodOpButton.addActionListener(listenForProdButton);
    }

    public void addOrderOppListener(ActionListener listenForOrdertButton){
        orderOpButton.addActionListener(listenForOrdertButton);
    }



}
