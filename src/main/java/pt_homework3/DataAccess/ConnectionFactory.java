package pt_homework3.DataAccess;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;

/**
 * Class responsible for making the connection with he database
 */
public class ConnectionFactory {

    private static final Logger LOGGER = Logger.getLogger(ConnectionFactory.class.getName());
    private static final String DRIVER = "com.mysql.jdbc.Driver";
    private static final String DBURL = "jdbc:mysql://localhost:3306/warehouse";

    private static ConnectionFactory singleInstance = new ConnectionFactory();

    private ConnectionFactory(){
        try {
            Class.forName(DRIVER);
        }catch(Exception e) {
            System.out.println("Unable to load Driver");
        }
    }

    public Connection createConnection(){
        //Establish connection using DriverManage
        try {
            Connection connection =
                    DriverManager.getConnection(DBURL, "root", "mozaic");
            return connection;
        } catch (SQLException e) {
            System.out.println("Unable to connect to database");
        }
        return null;
    }

    /**
     * Get connection
     * @return
     * @throws SQLException
     */
    public static Connection getConnection() throws SQLException {
        return singleInstance.createConnection();
    }

    /**
     * Close connection
     * @param connection
     * @throws SQLException
     */
    public static void close(Connection connection) throws SQLException {
        connection.close();
    }

    /**
     * close Statement
     * @param statement
     */
    public static void close(Statement statement){
        try {
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Close result
     * @param resultSet
     * @throws SQLException
     */
    public static void close(ResultSet resultSet) throws SQLException {
        resultSet.close();
    }





}