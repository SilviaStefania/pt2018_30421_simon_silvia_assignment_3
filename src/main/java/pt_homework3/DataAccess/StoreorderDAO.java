package pt_homework3.DataAccess;

import pt_homework3.Model.Clients;
import pt_homework3.Model.Products;
import pt_homework3.Model.Storeorder;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class StoreorderDAO {


    Logger LOGGER = Logger.getLogger(pt_homework3.DataAccess.StoreorderDAO.class.getName());

    List<Storeorder> storeorder;

    public List<Storeorder> getStoreorder() {
        return storeorder;
    }

    private String mess = "";

    private int nrOfRows;

    public int getNrOfRows() {
        return nrOfRows;
    }


    public StoreorderDAO() throws InvocationTargetException, SQLException, IntrospectionException, IllegalAccessException {
        this.storeorder = findAllStoreorder();
        this.nrOfRows = storeorder.size();
    }


    private String getStatementString(String field){
        String x="";
        x+="SELECT * FROM ";
        x+="storeorder";
        x+=" WHERE ";
        x+=field;
        x+=" =?";
        return x;
    }

    /**
     * Executes the query for finding an item after its id
     * @param id
     * @return
     * @throws SQLException
     * @throws IllegalAccessException
     * @throws IntrospectionException
     * @throws InvocationTargetException
     */
    public Storeorder findStoreById(int id) throws SQLException, IllegalAccessException, IntrospectionException, InvocationTargetException {

        int id1;
        int clientID;
        int productID;
        int quantity;

        Connection connection = ConnectionFactory.getConnection();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query  = getStatementString("id");

        try{

            statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            resultSet.next();
            id1 = Integer.parseInt(resultSet.getString(1));
            clientID =resultSet.getInt(2);
            productID = resultSet.getInt(3);
            quantity= resultSet.getInt(4);
            System.out.println(id1+clientID+productID+quantity);
            return(new Storeorder(id1, clientID, productID, quantity));

            //System.out.println(id1 +" "+ category +" "+ name +" "+ quantity+"\n " );



        } catch (SQLException e){
            LOGGER.log(Level.WARNING,  "DAO:find by id order " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }

        return null;
    }


    /**
     * Executes the query for inserting an item
     * @param clientID
     * @param productID
     * @param quantity
     * @return
     * @throws SQLException
     * @throws IllegalAccessException
     * @throws IntrospectionException
     * @throws InvocationTargetException
     */
    public List<Storeorder> insertOrderDB(int clientID, int productID, int quantity) throws SQLException, IllegalAccessException, IntrospectionException, InvocationTargetException {
        Connection connection = ConnectionFactory.getConnection();
        PreparedStatement statement = null;
        String query  = "INSERT INTO storeorder VALUES (NULL, ?, ?, ?)";

        try{
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, clientID);
            statement.setInt(2, productID);
            statement.setInt(3, quantity);
            statement.executeUpdate();
            System.out.println("Insert order ok");
            Storeorder aux = new Storeorder(clientID, productID, quantity);

            //pentru a decrementa nr de produse

            ProductsDAO prod = new ProductsDAO();
            List<Products> p = prod.findAllProducts();
            Iterator<Products> itt = p.iterator();
            while(itt.hasNext()){
                Products auxi = itt.next();
                if(auxi.getId() == productID){
                    if(auxi.getQuantity() >= quantity){
                        int newQuant = auxi.getQuantity() - quantity;
                        auxi.setQuantity(newQuant);
                        storeorder.add(aux);
                        mess = "Sufficient stock";
                    } else if(auxi.getQuantity() == 0){
                        mess = "Not enough products on stock. Select less";
                    } else {
                        mess = "Zero products left";
                    }

                }
            }


            return storeorder;

        } catch (SQLException e){
            LOGGER.log(Level.WARNING,  "INSERTION faill order " + e.getMessage());
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }

        return null;
    }


    /**
     * Executes the query for deleting an item after its id
     * @param id
     * @return
     * @throws SQLException
     */
    public List<Storeorder> deleteOrderDB(int id) throws SQLException {
        Connection connection =null;
        PreparedStatement statement = null;
        String query  = "DELETE FROM storeorder WHERE id = ?";

        try{
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            statement.executeUpdate();

            Iterator<Storeorder> it = this.storeorder.iterator();
            while(it.hasNext()){
                Storeorder paux = null;
                paux = it.next();
                if(paux.getId() == id){
                    it.remove();
                }
            }
            System.out.println("Deletion order succsesfull");
            return storeorder;


        } catch (SQLException e){
            LOGGER.log(Level.WARNING,  "DELETION faill order " + e.getMessage());
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }


    /**
     * Executes the query for editing an item after its id
     * @param id
     * @param fieldToUpd
     * @param newValue
     * @return
     * @throws SQLException
     */
    public List<Storeorder> updateOrderDB(int id, String fieldToUpd, String newValue ) throws SQLException {
        Connection connection =null;
        PreparedStatement statement = null;
        String query  = "UPDATE storeorder SET "+fieldToUpd+"='"+ newValue+"' WHERE id = ?";
        storeorder = new ArrayList<>();

        try{
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            statement.executeUpdate();

            for (Storeorder product : this.storeorder) {
                Storeorder caux = null;
                caux = product;
                if ("clientID" == fieldToUpd) {
                    caux.setClientID(Integer.parseInt(newValue));
                } else if ("productID" == fieldToUpd) {
                    caux.setProductID(Integer.parseInt(newValue));
                }
            }
            System.out.println("UPDATE order ok");
            return storeorder;
        } catch (SQLException e){
            LOGGER.log(Level.WARNING,  "UPDATEing faill prod " + e.getMessage());
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    private ArrayList<Object> makeObjList = new ArrayList<>();

    public ArrayList<Object> getMakeObjList() {
        return makeObjList;
    }

    /**
     * Executes the query for finding all the items
     * @return
     * @throws SQLException
     * @throws IllegalAccessException
     * @throws IntrospectionException
     * @throws InvocationTargetException
     */
    public List<Storeorder> findAllStoreorder() throws SQLException, IllegalAccessException, IntrospectionException, InvocationTargetException {
        Connection connection =null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Storeorder> initialList = new ArrayList<>();

        int id1;
        int clientID;
        int productID;
        int quan;

        try{
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement("SELECT * FROM storeorder");
            resultSet = statement.executeQuery();
            while(resultSet.next()){
                id1 = Integer.parseInt(resultSet.getString(1));
                clientID =resultSet.getInt(2);
                productID = resultSet.getInt(3);
                quan = resultSet.getInt(4);
                Storeorder aux = new Storeorder(id1, clientID, productID, quan);
                Object o = aux;
                this.makeObjList.add(o);
                initialList.add(aux);
                System.out.println(id1 +" "+ clientID +" "+ productID +"\n " );
            }

            // retriveProperties(resultSet);
            //return createObject(resultSet).get(0);

                return initialList;

        } catch (SQLException e){
            LOGGER.log(Level.WARNING,  "FIND ALL storeorder order " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }

        return null;

    }

    private int getTotalOrders(){
        return storeorder.size();
    }

}



