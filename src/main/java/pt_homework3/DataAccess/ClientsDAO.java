package pt_homework3.DataAccess;

import com.sun.xml.internal.org.jvnet.mimepull.CleanUpExecutorFactory;
import pt_homework3.Model.Clients;
import pt_homework3.Model.Products;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Class used to retrive data from the database and to run the specific queries
 */
public class ClientsDAO  {



    Logger LOGGER = Logger.getLogger(ClientsDAO.class.getName());

    private List<Clients> clients =new ArrayList<>();
    private int nrOfRows;

    public int getNrOfRows() {
        return nrOfRows;
    }

    public ClientsDAO() throws InvocationTargetException, SQLException, IntrospectionException, IllegalAccessException {
        this.clients = findAllClients();
        this.nrOfRows = clients.size();
    }

    public List<Clients> getClients() {
        return clients;
    }

    private String getStatementString(String field){
        String x="";
        x+="SELECT * FROM ";
        x+="clients";
        x+=" WHERE ";
        x+=field;
        x+=" =?";
        return x;
    }


    /**
     * Executes the query for finding an item after its id
     * @param id
     * @return
     * @throws SQLException
     * @throws IllegalAccessException
     * @throws IntrospectionException
     * @throws InvocationTargetException
     */
    public Clients findById(int id) throws SQLException, IllegalAccessException, IntrospectionException, InvocationTargetException {
        Connection connection = ConnectionFactory.getConnection();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query  = getStatementString("id");


        int id1;
        String name;
        String email;
        String address;

        try{

            statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            resultSet.next();
            id1 = Integer.parseInt(resultSet.getString(1));
            name =resultSet.getString(2);
            email = resultSet.getString(3);
            address= resultSet.getString(4);
            System.out.println(id1+name+email+address);
            Clients nn = new Clients(id1, name, email, address);
            return(nn);


        // retriveProperties(resultSet);
        //return createObject(resultSet).get(0);


        } catch (SQLException e){
            LOGGER.log(Level.WARNING,  "DAO:find by id clients " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }

      return  null;
    }


    /**
     * * Executes the query for inserting an item
     * @param name
     * @param email
     * @param address
     * @return
     * @throws SQLException
     */
    public List<Clients> insertClientDB(String name, String email, String address) throws SQLException {
       Connection connection= ConnectionFactory.getConnection();
        PreparedStatement statement = null;
        String query  = "INSERT INTO clients VALUES (NULL, ?, ?, ?)";

        try{
            statement = connection.prepareStatement(query);
            statement.setString(1, name);
            statement.setString(2, email);
            statement.setString(3, address);
            statement.executeUpdate();
            System.out.println("Insert client ok");
            clients.add(new Clients(name, email, address));

            return clients;

        } catch (SQLException e){
            LOGGER.log(Level.WARNING,  "INSERTION faill clients " + e.getMessage());
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }

        return null;

    }


    /**
     * * Executes the query for deleting an item
     * @param id
     * @return
     * @throws SQLException
     */
    public List<Clients> deleteClientDB(int id) throws SQLException {
        Connection connection =null;
        PreparedStatement statement = null;
        String query  = "DELETE FROM clients WHERE id = ?";
        clients = new ArrayList<>();

        try{
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            statement.executeUpdate();

            Iterator<Clients> it = this.clients.iterator();
            while(it.hasNext()){
                Clients caux = null;
                caux = it.next();
                if(caux.getId() == id){
                    it.remove();
                }
            }

            System.out.println("Deletion client succsesfull");
            return clients;


        } catch (SQLException e){
            LOGGER.log(Level.WARNING,  "DELETION faill clients " + e.getMessage());
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
return null;
    }


    /**
     * * Executes the query for editing an item
     * @param id
     * @param fieldToUpd
     * @param newValue
     * @return
     * @throws SQLException
     */
    public List<Clients> updateClientDB(int id, String fieldToUpd, String newValue ) throws SQLException {
        Connection connection =null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query  = "UPDATE clients SET "+fieldToUpd+"='"+ newValue+"' WHERE id = ?";
        clients = new ArrayList<>();

        try{
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            statement.executeUpdate();
            System.out.println("Update client ok");

            Iterator<Clients> it = this.clients.iterator();
            while(it.hasNext()){
                Clients caux = null;
                caux = it.next();
                if("name" == fieldToUpd){
                    caux.setName(newValue);
                }else
                if("address" == fieldToUpd){
                    caux.setAddress(newValue);
                }else
                if("email" == fieldToUpd){
                    caux.setEmail(newValue);
                }
            }

             return clients;

        } catch (SQLException e){
            LOGGER.log(Level.WARNING,  "UPDATEing faill clients " + e.getMessage());
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
            return null;
    }

    private ArrayList<Object> makeObjList = new ArrayList<>();

    public ArrayList<Object> getMakeObjList() {
        return makeObjList;
    }

    /**
     * * Executes the query for finding all the items
     * @return
     * @throws SQLException
     * @throws IllegalAccessException
     * @throws IntrospectionException
     * @throws InvocationTargetException
     */
    public ArrayList<Clients> findAllClients() throws SQLException, IllegalAccessException, IntrospectionException, InvocationTargetException {
        Connection connection =null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        ArrayList<Clients> initialList = new ArrayList<>();

        int id1;
        String name;
        String email;
        String address;

        try{
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement("SELECT * FROM clients");
            resultSet = statement.executeQuery();
            while(resultSet.next()){
                id1 = Integer.parseInt(resultSet.getString(1));
                name =resultSet.getString(2);
                email = resultSet.getString(3);
                address= resultSet.getString(4);
                Clients aux = new Clients(id1, name, email, address);
                Object o = aux;
                this.makeObjList.add(o);
                initialList.add(aux);
                System.out.println(id1+" "+ name +" "+ email +" "+address+"\n " );   //ca sa vad pe consola
            }


            return initialList;

            // retriveProperties(resultSet);
            //return createObject(resultSet).get(0);


        } catch (SQLException e){
            LOGGER.log(Level.WARNING,  "FIND ALL CLIENTS clients " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }

         return  null;
    }


    public List<Clients> getByHomeContinent(boolean homeCont) throws SQLException, IntrospectionException, IllegalAccessException, InvocationTargetException {
        ClientsDAO c = new ClientsDAO();
        //if homeCont display europa, else the rest
        List<Clients> cList = c.findAllClients();
        List<Clients> homeCl = new ArrayList<>();
        List<Clients> outsideCl = new ArrayList<>();
        Iterator<Clients> it = clients.iterator();
        while (it.hasNext()){
            Clients aux = it.next();
            if(aux.getAddress() == "Europe"){
                homeCl.add(aux);
            }
            else outsideCl.add(aux);
        }
        if(homeCont == true) return homeCl;
        else return outsideCl;
    }


}
