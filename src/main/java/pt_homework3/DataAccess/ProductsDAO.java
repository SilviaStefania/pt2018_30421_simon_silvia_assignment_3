package pt_homework3.DataAccess;

import pt_homework3.Model.Clients;
import pt_homework3.Model.Products;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProductsDAO  {


    Logger LOGGER = Logger.getLogger(pt_homework3.DataAccess.ProductsDAO.class.getName());

    List<Products> products = new ArrayList<>();
    private int nrOfRows;

    public int getNrOfRows() {
        return nrOfRows;
    }

    public ProductsDAO() throws InvocationTargetException, SQLException, IntrospectionException, IllegalAccessException {
            this.products = findAllProducts();
            this.nrOfRows = products.size();
    }


    private String getStatementString(String field){
        String x="";
        x+="SELECT * FROM ";
        x+="products";
        x+=" WHERE ";
        x+=field;
        x+=" =?";
        return x;
    }

    public List<Products> getProducts() {
        return products;
    }

    /**
     *  Executes the query for finding an item after its id
     * @param id
     * @return
     * @throws SQLException
     * @throws IllegalAccessException
     * @throws IntrospectionException
     * @throws InvocationTargetException
     */
      public Products findProductById(int id) throws SQLException, IllegalAccessException, IntrospectionException, InvocationTargetException {

        int id1;
        String category;
        String name;
        int quantity;
        int price;


        Connection connection = ConnectionFactory.getConnection();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query  = getStatementString("id");

        try{

            statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            resultSet.next();
            id1 = Integer.parseInt(resultSet.getString(1));
            category =resultSet.getString(2);
            name = resultSet.getString(3);
            quantity= resultSet.getInt(4);
            price = resultSet.getInt(5);
            System.out.println(id1+category+name+quantity);
            return(new Products(id1, category, name, quantity, price));

            //System.out.println(id1 +" "+ category +" "+ name +" "+ quantity+"\n " );



        } catch (SQLException e){
            LOGGER.log(Level.WARNING,  "DAO:find by id prod " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }

       return null;
    }


    /**
     *  * Executes the query for inserting an item
     * @param category
     * @param name
     * @param quantity
     * @param price
     * @return
     * @throws SQLException
     */
    public List<Products> insertProductDB(String category, String name, int quantity, int price) throws SQLException {
        Connection connection = ConnectionFactory.getConnection();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query  = "INSERT INTO products VALUES (NULL, ?, ?, ?, ?)";

        try{
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setString(1, category);
            statement.setString(2, name);
            statement.setInt(3, quantity);
            statement.setInt(4, price);
            statement.executeUpdate();
            System.out.println("Isert product ok");
            products.add(new Products(category, name, quantity, price));

            return products;

        } catch (SQLException e){
            LOGGER.log(Level.WARNING,  "INSERTION faill prod " + e.getMessage());
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }

        return null;
    }


    /**
     * * Executes the query for deleting an item
     * @param id
     * @return
     * @throws SQLException
     */
    public List<Products> deleteProductDB(int id) throws SQLException {
        Connection connection =null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query  = "DELETE FROM products WHERE id = ?";

        try{
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            statement.executeUpdate();

            Iterator<Products> it = this.products.iterator();
            while(it.hasNext()){
                Products paux = null;
                paux = it.next();
                if(paux.getId() == id){
                  it.remove();
                }
            }
            System.out.println("Deletion product succsesfull");
            return products;


        } catch (SQLException e){
            LOGGER.log(Level.WARNING,  "DELETION faill prod " + e.getMessage());
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
return null;
    }


    /**
     * * Executes the query for updating an item
     * @param id
     * @param fieldToUpd
     * @param newValue
     * @return
     * @throws SQLException
     */
    public List<Products> updateProductDB(int id, String fieldToUpd, String newValue ) throws SQLException {
        Connection connection =null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query  = "UPDATE products SET "+fieldToUpd+"='"+ newValue+"' WHERE id = ?";
        products = new ArrayList<>();

        try{
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            statement.executeUpdate();

            for (Products product : this.products) {
                Products caux = null;
                caux = product;
                if ("name" == fieldToUpd) {
                    caux.setName(newValue);
                } else if ("category" == fieldToUpd) {
                    caux.setCategory(newValue);
                } else if ("quantity" == fieldToUpd) {
                    caux.setQuantity(Integer.parseInt(newValue));
                } else if("price" == fieldToUpd){
                    caux.setPrice(Integer.parseInt(newValue));
                }
            }

            System.out.println("UPDATE product ok");
             return products;
        } catch (SQLException e){
            LOGGER.log(Level.WARNING,  "UPDATEing faill prod " + e.getMessage());
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
return null;
    }

    private ArrayList<Object> makeObjList = new ArrayList<>();

    public ArrayList<Object> getMakeObjList() {
        return makeObjList;
    }

    /**
     * * Executes the query for finding all the items
     * @return
     * @throws SQLException
     * @throws IllegalAccessException
     * @throws IntrospectionException
     * @throws InvocationTargetException
     */
    public List<Products> findAllProducts() throws SQLException, IllegalAccessException, IntrospectionException, InvocationTargetException {
        Connection connection =null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Products> initialList = new ArrayList<>();

        int id1;
        String category;
        String name;
        int quantity;
        int price;



        try{
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement("SELECT * FROM products");
            resultSet = statement.executeQuery();
            while(resultSet.next()){
                id1 = Integer.parseInt(resultSet.getString(1));
                category =resultSet.getString(2);
                name = resultSet.getString(3);
                quantity= resultSet.getInt(4);
                price = resultSet.getInt(5);
                Products aux = new Products(id1, category, name, quantity, price);
                Object o = aux;
                this.makeObjList.add(o);
                initialList.add(aux);
                System.out.println(id1 +" "+ category +" "+ name +" "+ quantity+" "+price +"\n" );
            }
            return initialList;

            // retriveProperties(resultSet);
            //return createObject(resultSet).get(0);


        } catch (SQLException e){
            LOGGER.log(Level.WARNING,  "FIND ALL products prod " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }

        return null;

    }


}



